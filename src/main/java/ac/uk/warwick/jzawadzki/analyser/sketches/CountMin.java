package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class CountMin extends AbstractSimpleSketch {

	protected int[][] data;
	protected MultiHasher hasher;
	protected int depth, width;

	public CountMin(int depth, int width, MultiHasher hasher) {
		super();
		this.data = new int[depth][width];
		this.hasher = hasher;
		this.depth = depth;
		this.width = width;
		System.out.println(width + "/" + depth);
	}

	@Override
	public void add(int object) {
		for (int d = 0; d < this.depth; d++)
			this.data[d][this.hasher.hash(d, object, this.width)]++;
	}

	@Override
	public int get(int object) {
		int min = this.data[0][this.hasher.hash(0, object, this.width)];
		for (int d = 1; d < this.depth; d++)
			min = Math.min(min, this.data[d][this.hasher.hash(d, object, this.width)]);
		return min;
	}

	@Override
	public String getName() {
		return "CM";//(d=" + this.depth + ")";
	}
}
