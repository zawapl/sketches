package ac.uk.warwick.jzawadzki.analyser.tester;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.sketches.Sketch;

public class SketchTester {

	private static final Logger LOGGER = Logger.getLogger(SketchTester.class);

	private File[] inputFiles;
	private SketchWriter sketchWriter;

	public SketchTester(File[] inputFiles, SketchWriter sketchWriter) {
		super();
		this.inputFiles = inputFiles;
		this.sketchWriter = sketchWriter;

	}

	public void test(Sketch sketch) {
		LOGGER.info("Testing " + sketch.getName());

		sketch.init();
		LOGGER.info("Populating sketch");
		long buildTime = 0;

		for (File inputFile : this.inputFiles)
			buildTime += this.populateSketch(inputFile, sketch);

		LOGGER.info("Sketch populated");
		sketch.decode();
		LOGGER.info("Sketch decoded");

		long getTime = this.sketchWriter.saveSketchCounts(sketch);
		this.sketchWriter.saveTimes(sketch.getName(), buildTime, getTime);

		LOGGER.info("Testing " + sketch.getClass().getSimpleName() + " completed.");
	}

	private long populateSketch(File inputFile, Sketch sketch) {
		long buildTime = 0;
		try (FileInputStream fIn = new FileInputStream(inputFile);
				BufferedInputStream bIn = new BufferedInputStream(fIn, 1048576);
				DataInputStream dataIn = new DataInputStream(bIn)){

			while (dataIn.available() > 0) {
				int hash = dataIn.readInt();
				long a = System.currentTimeMillis();
				sketch.add(hash);
				long b = System.currentTimeMillis();
				buildTime += b - a;
			}
		} catch (IOException e) {
			LOGGER.error("IOException when reading a file", e);
		} catch (Throwable t) {
			LOGGER.error("Throwable when populatingSketch a file", t);
			throw new IllegalArgumentException(t);
		}
		return buildTime;
	}

	public SketchWriter getWriter() {
		return this.sketchWriter;
	}

}
