package ac.uk.warwick.jzawadzki.analyser.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * Construction inspired by http://www.programcreek.com/2013/10/efficient-counter-in-java/
 *
 * @author jzawadzk
 */
public class WordCounter {

	private Map<Integer,WordCounter.Entry> counterByHash;
	private int uniquewords;
	private int totalCount;
	private StringHasher hasher;

	public WordCounter(int initialSize, StringHasher hasher){
		this.counterByHash = new HashMap<>(initialSize);
		this.hasher = hasher;
		this.uniquewords = 0;
		this.totalCount = 0;
	}

	public int count(String word){
		int hash = this.hasher.intHash(word);
		Entry entry = this.counterByHash.get(hash);
		if(entry == null){
			entry = new Entry();
			this.counterByHash.put(hash, entry);
		}
		if(!entry.words.contains(word)){
			entry.words.add(word);
			this.uniquewords++;
		}
		entry.count++;
		this.totalCount++;
		return hash;
	}

	public int getUniqueWords(){
		return this.uniquewords;
	}

	public Set<Integer> getHahses(){
		return this.counterByHash.keySet();
	}

	public int getCount(int hash) {
		Entry entry = this.counterByHash.get(hash);
		//		if(entry == null)
		//			return 0;
		return entry.count;
	}

	public String rowToString(int hash){
		Entry entry = this.counterByHash.get(hash);
		//		if(entry == null)
		//			entry = new Entry();
		return String.format("%d,%s,%d", hash, entry.getWords(), entry.count);
	}

	public int getTotalCount() {
		return this.totalCount;
	}


	private class Entry {

		List<String> words;
		int count;

		Entry(){
			this.words = new LinkedList<>();
			this.count = 0;
		}

		String getWords(){
			return StringUtils.join(this.words, '+');
		}

	}
}
