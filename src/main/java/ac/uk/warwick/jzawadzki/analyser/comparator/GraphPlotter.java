package ac.uk.warwick.jzawadzki.analyser.comparator;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.plot.AbstractPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.SVGTerminal;

public class GraphPlotter {

	private static final Logger LOGGER = Logger.getLogger(GraphPlotter.class);

	private String outputFolder;

	public GraphPlotter(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public void plotTimeVsErrors(Map<String, double[][]> dataSets, String type) {
		JavaPlot p = buildNewPlot("Average erros vs " + type + " time", type + "-time-vs-error.svg");
		setAxis(p, "time in seconds", "% error (log)", false, true);
		addAll(p, dataSets, Style.POINTS);
		p.plot();
	}

	public void plotFrequencyDistribution(double[][] data){
		AbstractPlot plot = new DataSetPlot(data);
		plot.setPlotStyle(new PlotStyle(Style.BOXES));
		plot.setTitle("Original text");

		JavaPlot p = buildNewPlot("Word frequency distribution", "freq-distr.svg");
		setAxis(p, "word frequency", "count", true, true);

		p.set("yrange", "[0.5:]");
		p.set("xrange", "[0.5:]");

		p.addPlot(plot);

		p.plot();
	}

	public void plotFreqVsError(Map<String, double[][]> dataSets, String range, boolean xLog, boolean yLog){
		JavaPlot p = buildNewPlot("Relative error vs frequency", "freq-vs-error" + range.replace(':', '-') + ".svg");
		String xAxis = xLog ? "word frequency (log)" : "word frequency";
		String yAxis = yLog ? "% error (log)" : "% error";
		setAxis(p, xAxis, yAxis, xLog, yLog);
		p.set("xrange", "[" + range + "]");
		addAll(p, dataSets, Style.LINES);
		p.plot();
	}

	private void addAll(JavaPlot p, Map<String, double[][]> dataSets, Style style){
		PlotStyle plotStyle = new PlotStyle(style);
		for(String sketch: dataSets.keySet()){
			AbstractPlot plot = new DataSetPlot(dataSets.get(sketch));
			plot.setPlotStyle(plotStyle);
			plot.setTitle(sketch);
			p.addPlot(plot);
		}
	}

	private void setAxis(JavaPlot p, String xLabel, String yLabel, boolean xLog, boolean yLog){
		p.set("xlabel", "'" + xLabel + "'");
		p.set("ylabel", "'" + yLabel + "'");
		p.getAxis("y").setLogScale(yLog);
		p.getAxis("x").setLogScale(xLog);
	}

	private JavaPlot buildNewPlot(String title, String filename){
		JavaPlot p = new JavaPlot();
		p.setTerminal(prepareOutputFile(filename));
		p.setTitle(title);
		return p;
	}

	private SVGTerminal prepareOutputFile(String filename){
		String path = this.outputFolder + File.separator + filename;
		LOGGER.debug("Preaparing " + path + " for writing");
		try {
			FileUtils.write(new File(path), "");
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		return new SVGTerminal(path);
	}

}
