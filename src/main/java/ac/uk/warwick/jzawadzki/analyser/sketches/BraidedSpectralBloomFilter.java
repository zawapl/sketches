package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class BraidedSpectralBloomFilter implements Sketch {

	private MultiHasher hasher;
	private byte[][] data;
	private int[] k;
	private int[] size;

	public BraidedSpectralBloomFilter(MultiHasher hasher, int[] k, int[] size) {
		this.hasher = hasher;
		this.size = size;
		this.k = k;
	}

	@Override
	public void init() {
		if(this.k.length != this.size.length)
			throw new IllegalArgumentException("Lengths of k and size don't match");

		this.data = new byte[this.k.length][];
		for(int i = 0; i < this.k.length; i++)
			this.data[i] = new byte[this.size[i]];
	}

	@Override
	public void add(int object) {
		addToLayer(object, 0);
	}

	private void addToLayer(int object, int layer) {
		for(int i = 0; i < this.k[layer]; i++) { 						// Do it k times
			int hash = this.hasher.hash(i, object, this.size[layer]);			// Get pointer for this k
			if(this.data[layer][hash] == -1 || this.data[layer][hash] == Byte.MAX_VALUE) {	// If about to overflow
				this.data[layer][hash] = Byte.MIN_VALUE;					// Reset to 0
				addToLayer(hash, layer + 1);							// Increment next layer
			} else										// If not overflowing
				this.data[layer][hash]++;							// Increment this layer
		}
	}

	@Override
	public int get(int object) {
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < this.k[0]; i++)
			min = Math.min(getValueOf(this.hasher.hash(i, object, this.size[0]), 0), min);
		return min;
	}

	private int getValueOf(int pointer, int layer) {
		int val = this.data[layer][pointer];
		if(val >= 0)
			return val;
		else {
			int min = Integer.MAX_VALUE;
			layer++;
			for(int i = 0; i < this.k[layer]; i++)
				min = Math.min(getValueOf(this.hasher.hash(i, pointer, this.size[layer]), layer), min);
			return (min << 7) + (val - Byte.MIN_VALUE);
		}
	}

	@Override
	public String getName() {
		return "BSBF";//(k=" + Arrays.toString(this.k) + ", size=" + Arrays.toString(this.size) + ")";
	}

	@Override
	public void decode() {
		// No-op
	}

}
