package ac.uk.warwick.jzawadzki.analyser.comparator;

import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

public class Verifier {

	private static final Logger LOGGER = Logger.getLogger(Verifier.class);

	public static boolean verifyCountMinAndCountMinCU(CSVParser countMinP, CSVParser countMinCUP){
		LOGGER.info("Verifying countMin and countMinCU");
		@SuppressWarnings("unchecked") Iterator<CSVRecord>[] iterators = new Iterator[]{
				countMinP.iterator(),
				countMinCUP.iterator()
		};
		int[] maxIncorrect = new int[4];
		boolean dataValid = true;
		while(dataValid && iterators[0].hasNext() && iterators[1].hasNext()){
			CSVRecord[] records = {iterators[0].next(), iterators[1].next()};
			if(records[0].get("Hash").equals(records[1].get("Hash")) && iterators[0].hasNext() == iterators[1].hasNext()){
				int[] counts = {
						Integer.parseInt(records[0].get("True count")),
						Integer.parseInt(records[0].get("Count")),
						Integer.parseInt(records[1].get("Count"))
				};
				dataValid =checkIfValid(counts, records);
				updateMax(counts, maxIncorrect);
			} else{
				LOGGER.warn("Objects do not match: " + Arrays.toString(records));
				dataValid = false;
			}
		}
		if(dataValid){
			LOGGER.info("Max incorrect for CountMin: " + maxIncorrect[0] + " vs " + maxIncorrect[1]);
			LOGGER.info("Max incorrect for CountMinCU: " + maxIncorrect[2] + " vs " + maxIncorrect[3]);
		} else
			LOGGER.warn("Verification failed");
		return dataValid;
	}

	private static boolean checkIfValid(int[] counts, Object[] records){
		if(counts[2] > counts[1])
			LOGGER.warn("CountMinCU worse than CountMin " + Arrays.toString(records));

		if(counts[0] > counts[2])
			LOGGER.warn("CountMinCU underestimated " + Arrays.toString(records));

		return (counts[0] <= counts[2] && counts[2] <= counts[1]);
	}

	private static void updateMax(int[] counts, int[] maxIncorrect){
		if(counts[0] != counts[1] && counts[0] > maxIncorrect[0]){
			maxIncorrect[0] = counts[0];
			maxIncorrect[1] = counts[1];
		}
		if(counts[0] != counts[2] && counts[0] > maxIncorrect[2]){
			maxIncorrect[2] = counts[0];
			maxIncorrect[3] = counts[2];
		}
	}
}
