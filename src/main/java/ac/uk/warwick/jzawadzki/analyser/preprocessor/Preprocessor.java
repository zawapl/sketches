package ac.uk.warwick.jzawadzki.analyser.preprocessor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.utils.StringHasher;
import ac.uk.warwick.jzawadzki.analyser.utils.WordCounter;

public class Preprocessor {

	private static final Logger LOGGER = Logger.getLogger(Preprocessor.class);

	private File[] inputFiles;
	private File outputFolder;
	private File counterFile;
	private File statsFile;
	private StringHasher stringHasher;
	private int initialSize;

	public Preprocessor(File[] i, File o, File c, File s, StringHasher h, int size) {
		super();
		this.outputFolder = o;
		this.stringHasher = h;
		this.inputFiles = i;
		this.statsFile = s;
		this.counterFile = c;
		this.initialSize = size;
	}

	public void startPreProcessing() {
		LOGGER.info("Preprocessing started");
		long start = System.currentTimeMillis();
		WordCounter counter = preprocess();
		long diff = System.currentTimeMillis() - start;
		LOGGER.info("Preprocessing finished");

		PreprocessorWriter writer = new PreprocessorWriter(this.statsFile, this.counterFile);

		writer.saveCounterFile(counter);
		writer.saveStatsFile(diff, counter);
	}

	private WordCounter preprocess() {
		WordCounter counter = new WordCounter(this.initialSize, this.stringHasher);
		for (File inputFile : this.inputFiles) {
			String fileName = this.outputFolder.getAbsolutePath() + File.separator + inputFile.getName();
			File outputFile = new File(fileName);
			try {
				processFile(counter, inputFile, outputFile);
			} catch(Throwable t){
				System.gc();
				LOGGER.error("Unexpected Throwable when processing a file", t);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {

				}
			}
		}
		return counter;
	}

	private void processFile(WordCounter counter, File inputFile, File outputFile) {
		LineIterator it = null;
		int i = 0;

		try ( FileOutputStream fOut = FileUtils.openOutputStream(outputFile);
				BufferedOutputStream bOut = new BufferedOutputStream(fOut);
				/*DataOutputStream dataOut = new DataOutputStream(bOut)*/ ) {
			it = FileUtils.lineIterator(inputFile, "UTF-8");
			String line = it.nextLine();
			do{
				if (++i % 1000000 == 0) { // Notify user of progress every 1M lines
					//dataOut.flush();
					bOut.flush();
					fOut.flush();
					System.gc();
					LOGGER.debug("Read " + i + " lines of " + inputFile + ", \t" + counter.getUniqueWords() + " unique words found with " + counter.getHahses().size() + " unique hashes.");
				}
				processLine(line, bOut, counter);
				line = it.nextLine();
			} while(line != null);
		} catch (IOException e){
			LOGGER.error("Exception while processing line " + i + " of " + inputFile, e);
		} catch (NoSuchElementException e){
			LOGGER.trace("Read " + i + " lines of " + inputFile + ", " + counter.getUniqueWords() + " unique words found with " + counter.getHahses().size() + " unique hashes.");
		} finally {
			LineIterator.closeQuietly(it);
		}
	}

	private void processLine(String line, OutputStream dataOut, WordCounter counter) throws IOException{
		String[] words = line.split(" ");
		for (String word : words) {
			String trimmed = word.trim();
			if (trimmed.length() > 0) {
				int hash = counter.count(trimmed);
				dataOut.write(ByteBuffer.allocate(4).putInt(hash).array());
			}
		}
	}
}
