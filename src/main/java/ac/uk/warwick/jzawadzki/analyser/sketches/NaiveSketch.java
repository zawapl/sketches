package ac.uk.warwick.jzawadzki.analyser.sketches;

import java.util.HashMap;
import java.util.Map;

public class NaiveSketch extends AbstractSimpleSketch {

	private Map<Integer, int[]> data;

	public NaiveSketch() {
		this.data = new HashMap<>();
	}

	@Override
	public void add(int object) {
		int[] values = this.data.get(object);
		if(values == null)
			this.data.put(object, new int[] { 1 });
		else
			values[0] += 1;
	}

	@Override
	public int get(int object) {
		int[] currentVal = this.data.get(object);
		if(currentVal == null)
			return 0;
		return currentVal[0];
	}

	@Override
	public String getName() {
		return "NaiveSketch";
	}
}
