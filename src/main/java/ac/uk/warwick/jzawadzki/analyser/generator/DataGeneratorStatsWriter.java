package ac.uk.warwick.jzawadzki.analyser.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.preprocessor.PreprocessorWriter;

public class DataGeneratorStatsWriter {

	private static final Logger LOGGER = Logger.getLogger(PreprocessorWriter.class);

	private File statsFile;
	private File counterFile;

	public DataGeneratorStatsWriter(File statsFile, File counterFile) {
		this.statsFile = statsFile;
		this.counterFile = counterFile;
	}

	public void saveCounterFile(Map<Integer, int[]> counter) {
		LOGGER.info("Saving counter file");

		try(FileWriter fWriter = new FileWriter(this.counterFile);
				BufferedWriter bWriter = new BufferedWriter(fWriter)){
			bWriter.write("Hash,Keywords,Count");
			for (Map.Entry<Integer, int[]> entry: counter.entrySet()){
				bWriter.newLine();
				bWriter.write(String.format("%d,%d,%d", entry.getKey(), entry.getKey(), entry.getValue()[0]));
			}
			LOGGER.info("Counter file saved to " + this.counterFile.getAbsolutePath());
		} catch (IOException e) {
			LOGGER.error("Error when appending to " + this.counterFile.getAbsolutePath(), e);
		}
	}

	public void saveStatsFile(long time, Map<Integer, int[]> counter) {
		LOGGER.info("Savign stats file");
		StringBuilder data = new StringBuilder();

		int min = Integer.MAX_VALUE;
		int max = 0;
		int sum = 0;

		for (Map.Entry<Integer, int[]> entry: counter.entrySet()) {
			int val = entry.getValue()[0];
			if(val < min)
				min = val;
			if(val > max)
				max = val;
			sum += val;
		}

		data.append(String.format("preprocess_time=%d", time));
		data.append(String.format("%nunique_words=%d", counter.size()));
		data.append(String.format("%nunique_hashes=%d", counter.size()));
		data.append(String.format("%ntotal_count=%d", sum));
		data.append(String.format("%nmax_value=%d", max));
		data.append(String.format("%nmin_value=%d", min));

		try {
			FileUtils.write(this.statsFile, data);
			LOGGER.info("Stas file saved to " + this.statsFile.getAbsolutePath());
		} catch (IOException e) {
			LOGGER.error("Error when saving to " + this.statsFile.getAbsolutePath(), e);
		}
	}

}
