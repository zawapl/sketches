package ac.uk.warwick.jzawadzki.analyser.utils;

/**
 * Using https://en.wikipedia.org/wiki/Fowler–Noll–Vo_hash_function
 *
 * "Research": http://programmers.stackexchange.com/questions/49550/which-hashing-algorithm-is-best-for-uniqueness-and-speed
 *
 * @author Kuba
 *
 */
public class StringHasher {

	private final long PRIME;
	private final long OFFSET;

	private final int INT_PRIME = 16777619;
	private final int INT_OFFSET = Integer.parseUnsignedInt("2166136261");

	public StringHasher() {
		this.PRIME = 1099511628211L;
		this.OFFSET = Long.parseUnsignedLong("14695981039346656037");
	}

	private long hash(String string) {
		long hash = this.OFFSET;
		byte[] bytes = string.getBytes();

		for (int i = 0; i < bytes.length; i++) {
			hash = hash ^ (bytes[i] & 0xff);
			hash = hash * this.PRIME;
		}

		return hash;
	}

	public int intHash(String string) {
		int hash = this.INT_OFFSET;
		byte[] bytes = string.getBytes();

		for (int i = 0; i < bytes.length; i++) {
			hash = hash ^ (bytes[i] & 0xff);
			hash = hash * this.INT_PRIME;
		}

		return hash;
	}
}
