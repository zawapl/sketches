package ac.uk.warwick.jzawadzki.analyser.sketches;

public abstract class AbstractSimpleSketch implements Sketch {

	@Override
	public void init() {
		// no-op
	}

	@Override
	public void decode() {
		// no-op
	}

}
