package ac.uk.warwick.jzawadzki.analyser.utils;

import java.util.Random;

public class MultiHasher {

	private long[] a;
	private long[] b;
	private final long PRIME;

	public MultiHasher(int size, long prime) {
		this.a = new long[size];
		this.b = new long[size];
		this.PRIME = prime;

	}

	public int hash(int i, long val, int lim) {
		int h = (int) ((this.a[i] * val + this.b[i]) % this.PRIME) % lim;
		while (h < 0)
			h += lim;
		return h;
	}

	public void randomise() {
		Random r = new Random(this.PRIME);
		for (int i = 0; i < this.a.length; i++) {
			do {
				this.a[i] = this.nextLong(r) % this.PRIME;
			} while (this.a[i] <= 0);
			do {
				this.b[i] = this.nextLong(r) % this.PRIME;
			} while (this.b[i] <= 0);
		}

	}

	private long nextLong(Random rng) {
		long bits, val;
		do {
			bits = (rng.nextLong() << 1) >>> 1;
			val = bits % this.PRIME;
		} while (bits - val + (this.PRIME - 1) < 0L);
		return val;
	}

}
