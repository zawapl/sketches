package ac.uk.warwick.jzawadzki.analyser.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;

public class Properties {

	private final static Logger LOGGER = Logger.getLogger(Properties.class);

	private java.util.Properties properties;

	public Properties() {
		this.properties = new java.util.Properties();
	}

	public void load(String filename) {
		try (InputStream input = new FileInputStream(filename)){
			this.properties.load(input);
		} catch (IOException e) {
			LOGGER.error("Failed to load properties from " + filename, e);
		}
	}

	public File[] getFiles(String key) {
		try {
			File dir = new File(this.getProperty(key));
			FileFilter fileFilter = new WildcardFileFilter("*");
			return dir.listFiles(fileFilter);
		} catch (SecurityException e){
			LOGGER.error("Failed to get files for " + key, e);
		}
		return new File[0];
	}

	public File getFile(String key) {
		return new File(getProperty(key));
	}

	public String[] getList(String key) {
		return this.getProperty(key).split(",");
	}

	public int getInt(String key, int def) {
		String num = this.getProperty(key);
		try{
			return Integer.valueOf(num);
		} catch (NumberFormatException e){
			LOGGER.error("Failed to parse '" + num + "' for " + key + ", returning " + def);
		}
		return def;
	}

	private String getProperty(String key) {
		String val = this.properties.getProperty(key, "");
		return val;
	}

}
