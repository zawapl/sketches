package ac.uk.warwick.jzawadzki.analyser.view;

import java.util.Scanner;

import ac.uk.warwick.jzawadzki.analyser.main.Application;

public class CommandLineView implements ApplicationView {

	@Override
	public void getUserDecision(Application app) {
		Scanner reader = new Scanner(System.in);
		String in;
		do {
			System.out.println("Choose your option(s):");
			System.out.println("1. Preprocessor");
			System.out.println("2. Tester");
			System.out.println("3. Comparator");
			System.out.println("4. Optimiser");
			System.out.println("5. Generator");
			System.out.println("0. Exit");
			in = reader.nextLine();
			for (byte b : in.getBytes()){
				switch (b) {
				case '1':
					app.runPreprocessor();
					break;
				case '2':
					app.runTester();
					break;
				case '3':
					app.runComparator();
					break;
				case '4':
					app.runOptimiser();
					break;
				case '5':
					app.runGenerator();
					break;
				case '0':
					reader.close();
					app.exit();
					return;
				default:
					System.out.println("Unrecognised option '" + b + "'");
				}
			}
		} while (true);
	}

}
