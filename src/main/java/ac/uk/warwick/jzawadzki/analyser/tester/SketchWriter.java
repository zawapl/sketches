package ac.uk.warwick.jzawadzki.analyser.tester;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.sketches.Sketch;

public class SketchWriter {

	private static final Logger LOGGER = Logger.getLogger(SketchWriter.class);

	private File counterFile;
	private File countsFolder;
	private File timesFolder;

	public SketchWriter(File counterFile, File countsFolder, File timesFolder) {
		this.counterFile = counterFile;
		this.countsFolder = countsFolder;
		this.timesFolder = timesFolder;
	}

	public long saveSketchCounts(Sketch sketch) {
		long time = 0;
		File output = getOutputFile(this.countsFolder, sketch.getName(), "csv");
		try {
			FileUtils.write(output, "");
			try(PrintWriter writer = new PrintWriter(output)){
				writer.println("Hash,Words,Count,True count");
				try(CSVParser parser = CSVParser.parse(this.counterFile, Charset.forName("UTF-8"), CSVFormat.DEFAULT.withHeader())){
					for(CSVRecord record: parser){
						int hash = Integer.parseInt(record.get("Hash"));
						long a = System.currentTimeMillis();
						int count = sketch.get(hash);
						long b = System.currentTimeMillis();
						time += b - a;
						writer.println(String.format("%d,%s,%d,%d", hash, record.get("Keywords"), count, Long.parseLong(record.get("Count"))));
					}
				}
			}
		} catch (IOException e){
			LOGGER.error("IOException when saving count", e);
		}

		LOGGER.info("Counts saved to " + output.getAbsolutePath());

		return time;
	}

	public File getCountsFolder() {
		return this.countsFolder; 
	}

	public void saveTimes(String filename, long build, long get) {
		File output = getOutputFile(this.timesFolder, filename, "txt");
		StringBuilder data = new StringBuilder();
		data.append("build_time=").append(build).append(System.getProperty("line.separator"));
		data.append("get_time=").append(get).append(System.getProperty("line.separator"));
		try {
			FileUtils.write(output, data);
		} catch (IOException e) {
			LOGGER.error("IOException when saving times file", e);
		}
	}

	public static File getOutputFile(File folder, String filename, String extension) {
		return new File(folder.getAbsolutePath() + File.separator + filename.replace(" ", "") + "." + extension);
	}

}
