package ac.uk.warwick.jzawadzki.analyser.view;

import ac.uk.warwick.jzawadzki.analyser.main.Application;

public interface ApplicationView {

	public void getUserDecision(Application app);

}
