package ac.uk.warwick.jzawadzki.analyser.sketches;

public interface Sketch {

	// Prepare sketch for inserting numbers (if needed)
	abstract void init();

	// Prepare sketch for reading numbers (if needed)
	abstract void decode();

	// Increment number of occurrences of the object by the value of the count
	abstract void add(int object);

	// Get number of recorded occurrences for this object
	abstract int get(int object);

	// Get a string decribing this sketch
	abstract String getName();
}
