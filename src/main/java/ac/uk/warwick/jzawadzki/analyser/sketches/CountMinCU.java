package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class CountMinCU extends CountMin {

	public CountMinCU(int depth, int width, MultiHasher hasher) {
		super(depth, width, hasher);
	}

	@Override
	public void add(int object) {
		int val = this.get(object) + 1;
		for (int d = 0; d < this.depth; d++) {
			int hash = this.hasher.hash(d, object, this.width);
			this.data[d][hash] = Math.max(val, this.data[d][hash]);
		}
	}

	@Override
	public String getName() {
		return "CM-CU";//(d=" + this.depth + ")";
	}

}
