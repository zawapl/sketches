package ac.uk.warwick.jzawadzki.analyser.main;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import ac.uk.warwick.jzawadzki.analyser.comparator.BucketCalculator;
import ac.uk.warwick.jzawadzki.analyser.comparator.Comparator;
import ac.uk.warwick.jzawadzki.analyser.comparator.GraphPlotter;
import ac.uk.warwick.jzawadzki.analyser.generator.DataGenerator;
import ac.uk.warwick.jzawadzki.analyser.generator.DataGeneratorStatsWriter;
import ac.uk.warwick.jzawadzki.analyser.generator.ZipfGenerator;
import ac.uk.warwick.jzawadzki.analyser.optimiser.Optimiser;
import ac.uk.warwick.jzawadzki.analyser.preprocessor.Preprocessor;
import ac.uk.warwick.jzawadzki.analyser.sketches.SketchFactory;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchTester;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchWriter;
import ac.uk.warwick.jzawadzki.analyser.utils.Properties;
import ac.uk.warwick.jzawadzki.analyser.utils.StringHasher;
import ac.uk.warwick.jzawadzki.analyser.view.ApplicationView;
import ac.uk.warwick.jzawadzki.analyser.view.CommandLineView;

public class Application {

	private final static Logger LOGGER = Logger.getLogger(Application.class);

	private ApplicationView view;
	private ResourceManager resources;

	public Application() {
		this.resources = ResourceManager.getDefault();
		this.view = new CommandLineView();
	}

	public void runTester() {
		{
			Properties props = this.resources.getProperties();

			File[] inputFiles = props.getFiles("preprocessed_binary");
			File counterFile = props.getFile("preprocessed_counter");
			File outputCounts = props.getFile("sketch_counts");
			File outputTimes = props.getFile("sketch_times");

			SketchWriter sketchWriter = new SketchWriter(counterFile, outputCounts, outputTimes);
			SketchTester st = new SketchTester(inputFiles, sketchWriter);
			st.test(this.resources.getSketchFactory().getSpectralBloomFilterSketch());
			st.test(this.resources.getSketchFactory().getCountMinSketch());
			st.test(this.resources.getSketchFactory().getSpectralBloomFilterCUSketch());
			st.test(this.resources.getSketchFactory().getCountMinCUSketch());
			st.test(this.resources.getSketchFactory().getSpectralBloomFilterBraidedSketch());
			st.test(this.resources.getSketchFactory().getSpectralBloomFilterRMSketch());
		}
		System.gc();
	}

	public void runPreprocessor() {
		{
			Properties props = this.resources.getProperties();
			StringHasher hasher = this.resources.getStringHasher();

			File[] inputFiles = props.getFiles("raw_folder");
			File outputfolder = props.getFile("preprocessed_binary");
			File counterFile = props.getFile("preprocessed_counter");
			File statsFile = props.getFile("preprocessed_stats");
			int initialSize = props.getInt("preprocessor_counter_size", 16);

			Preprocessor p = new Preprocessor(inputFiles, outputfolder, counterFile, statsFile, hasher, initialSize);
			p.startPreProcessing();
		}
		System.gc();
	}

	public void runComparator() {
		{
			Properties props = this.resources.getProperties();

			File[] sketchCounters = props.getFiles("sketch_counts");
			String gnuplotOut = props.getFile("gnuplot_out").getAbsolutePath();
			int buckets = props.getInt("comparator_buckets", 128);
			String sketchTimes = props.getList("sketch_times")[0];

			Properties stats = new Properties();
			stats.load(props.getList("preprocessed_stats")[0]);
			int max = stats.getInt("max_value", 100000000);

			BucketCalculator bucketCalculator = new BucketCalculator(buckets, max);
			GraphPlotter plotter = new GraphPlotter(gnuplotOut);

			Comparator comparator = new Comparator(sketchCounters, sketchTimes, plotter, bucketCalculator);
			comparator.createAllGraphs(false);
		}
		System.gc();
	}

	public void runOptimiser() {
		{
			SketchFactory factory = this.resources.getSketchFactory();
			Properties props = this.resources.getProperties();

			File[] inputFiles = props.getFiles("preprocessed_binary");
			File counterFile = props.getFile("preprocessed_counter");
			String optimiserOut = props.getList("optimiser_out")[0];
			int buckets = props.getInt("comparator_buckets", 128);

			Properties stats = new Properties();
			stats.load(props.getList("preprocessed_stats")[0]);
			int max = stats.getInt("max_value", 100000000);

			BucketCalculator bucketCalculator = new BucketCalculator(buckets, max);

			Optimiser optimiser = new Optimiser(factory, inputFiles, counterFile, optimiserOut, bucketCalculator);
			LOGGER.info("Optimal values for SpectralBloomFilterRM: " + optimiser.optimiseSpectralBloomRM());
			LOGGER.info("Optimal k for CountMin: " + optimiser.optimiseCountMin());
			LOGGER.info("Optimal k for CountMinCU: " + optimiser.optimiseCountMinCU());
			LOGGER.info("Optimal k for SpectralBloomFilter: " + optimiser.optimiseSpectralBloom());
			LOGGER.info("Optimal k for SpectralBloomFilterCU: " + optimiser.optimiseSpectralBloomCU());
			LOGGER.info("Optimal values for BraidedSpectralBloomFilter: " + optimiser.optimiseBraidedSpectralBloomRM());
		}
		System.gc();
	}

	public void runGenerator() {
		{
			Properties props = this.resources.getProperties();
			File outputfolder = props.getFile("generator_binary");
			File counterFile = props.getFile("generator_counter");
			File statsFile = props.getFile("generator_stats");
			int size = props.getInt("generator_length", 1000000);
			int range = props.getInt("generator_range", 1000000);

			DataGeneratorStatsWriter writer = new DataGeneratorStatsWriter(statsFile, counterFile);
			DataGenerator generator = new ZipfGenerator(outputfolder, writer, size, range);
			generator.generate();
		}
		System.gc();
	}

	public void exit(){
		LOGGER.info("Exiting");
		System.exit(0);
	}

	private static void configureLoggers() {
		Logger rootLogger = Logger.getRootLogger();
		rootLogger.setLevel(Level.TRACE);
		PatternLayout layout = new PatternLayout("%d{ISO8601} %-5p [%-35.35c:%-3.3L] - %m%n");
		rootLogger.addAppender(new ConsoleAppender(layout));
		try {
			RollingFileAppender fileAppender = new RollingFileAppender(layout, "logs" + File.separator + "log");
			rootLogger.addAppender(fileAppender);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}

	public static void main(String[] args) {
		configureLoggers();
		Application app = new Application();
		app.view.getUserDecision(app);
	}

}
