package ac.uk.warwick.jzawadzki.analyser.generator;

import java.io.File;
import java.util.Random;

import org.apache.commons.math3.distribution.ZipfDistribution;


public class ZipfGenerator extends DataGenerator {

	private ZipfDistribution distribution;

	public ZipfGenerator(File output, DataGeneratorStatsWriter writer, int size, int range) {
		super(output, writer, size, range);
		double exponent = 1;
		this.distribution = new ZipfDistribution((int) (range / exponent), exponent);
	}

	@Override
	protected int randomNumber(Random random, double a) {
		return this.distribution.sample();
	}

}
