package ac.uk.warwick.jzawadzki.analyser.main;

import ac.uk.warwick.jzawadzki.analyser.sketches.SketchFactory;
import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;
import ac.uk.warwick.jzawadzki.analyser.utils.Properties;
import ac.uk.warwick.jzawadzki.analyser.utils.StringHasher;

public class ResourceManager {

	private Properties properties;
	private StringHasher stringHasher;
	private MultiHasher hasher;
	private SketchFactory sketchFactory;

	private ResourceManager(Properties properties, StringHasher stringHasher,
			MultiHasher hasher, SketchFactory sketchFactory) {
		this.properties = properties;
		this.stringHasher = stringHasher;
		this.hasher = hasher;
		this.sketchFactory = sketchFactory;
	}

	public static ResourceManager getDefault() {
		Properties properties = new Properties();
		properties.load("default.properties");
		StringHasher stringHasher = new StringHasher();
		MultiHasher hasher = new MultiHasher(properties.getInt("hasher_depth", 5), 2147483647);
		hasher.randomise();
		SketchFactory sketchFactory = new SketchFactory(properties.getInt("sketch_size", 1), hasher);
		return new ResourceManager(properties, stringHasher, hasher, sketchFactory);
	}

	public Properties getProperties() {
		return this.properties;
	}

	public StringHasher getStringHasher() {
		return this.stringHasher;
	}

	public MultiHasher getMultiHasher() {
		return this.hasher;
	}

	public SketchFactory getSketchFactory() {
		return this.sketchFactory;
	}
}
