package ac.uk.warwick.jzawadzki.analyser.preprocessor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.utils.WordCounter;

public class PreprocessorWriter {

	private static final Logger LOGGER = Logger.getLogger(PreprocessorWriter.class);

	private File statsFile;
	private File counterFile;

	public PreprocessorWriter(File statsFile, File counterFile) {
		this.statsFile = statsFile;
		this.counterFile = counterFile;
	}

	public void saveStatsFile(long time, WordCounter counter) {
		LOGGER.info("Savign stats file");
		StringBuilder data = new StringBuilder();

		int min = Integer.MAX_VALUE;
		int max = 0;

		for (int hash: counter.getHahses()) {
			int val = counter.getCount(hash);
			if(val < min)
				min = val;
			if(val > max)
				max = val;
		}

		data.append(String.format("preprocess_time=%d", time));
		data.append(String.format("%nunique_words=%d", counter.getUniqueWords()));
		data.append(String.format("%nunique_hashes=%d", counter.getHahses().size()));
		data.append(String.format("%ntotal_count=%d", counter.getTotalCount()));
		data.append(String.format("%nmax_value=%d", max));
		data.append(String.format("%nmin_value=%d", min));

		try {
			FileUtils.write(this.statsFile, data);
			LOGGER.info("Stas file saved to " + this.statsFile.getAbsolutePath());
		} catch (IOException e) {
			LOGGER.error("Error when saving to " + this.statsFile.getAbsolutePath(), e);
		}
	}

	public void saveCounterFile(WordCounter counter) {
		LOGGER.info("Saving counter file");

		try(FileWriter fWriter = new FileWriter(this.counterFile);
				BufferedWriter bWriter = new BufferedWriter(fWriter)){
			bWriter.write("Hash,Keywords,Count");
			for (int hash: counter.getHahses()){
				bWriter.newLine();
				bWriter.write(counter.rowToString(hash));
			}
			LOGGER.info("Counter file saved to " + this.counterFile.getAbsolutePath());
		} catch (IOException e) {
			LOGGER.error("Error when appending to " + this.counterFile.getAbsolutePath(), e);
		}
	}

}
