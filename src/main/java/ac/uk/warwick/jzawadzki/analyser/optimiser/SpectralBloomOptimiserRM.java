package ac.uk.warwick.jzawadzki.analyser.optimiser;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.comparator.BucketCalculator;
import ac.uk.warwick.jzawadzki.analyser.sketches.Sketch;
import ac.uk.warwick.jzawadzki.analyser.sketches.SketchFactory;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchTester;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchWriter;

public class SpectralBloomOptimiserRM {

	private final static Logger LOGGER = Logger.getLogger(Optimiser.class);

	private SketchFactory factory;
	private BucketCalculator bucketCalculator;

	public SpectralBloomOptimiserRM(SketchFactory factory, BucketCalculator bucketCalculator) {
		this.factory = factory;
		this.bucketCalculator = bucketCalculator;
	}

	public String optimiseSpectralBloomRM(String optimiserOut, SketchTester tester) {
		int[] optimalK = {4,3};
		OptimisingDouble optimalRatio = new OptimisingDouble(0.218750);
		double optimalErr = Double.MAX_VALUE;

		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);

		while(true) {
			double tmpOptimal = optimalRatio.getVal();
			optimalRatio.decreaseStep();

			if(tmpOptimal != optimalRatio.getVal()) {
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, 1, 1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, 0, -1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, -1, 1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, 1, 0, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, -1, -1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, 0, 1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, 1, -1, optimalErr);
				optimalErr = this.testDirection(tester, countsFolder, optimalRatio, optimalK, -1, 0, optimalErr);
			}

			while(true) {
				double newVal = optimalRatio.getVal() - optimalRatio.getStep();
				double err = this.testSBFRM(tester, countsFolder, optimalK[0], optimalK[1], newVal);
				if(err < optimalErr) {
					optimalRatio.setVal(newVal);
					optimalErr = err;
					optimalRatio.decreaseStep();
				} else
					break;
			}

			while(true) {
				double newVal = optimalRatio.getVal() + optimalRatio.getStep();
				double err = this.testSBFRM(tester, countsFolder, optimalK[0], optimalK[1], newVal);
				if(err < optimalErr) {
					optimalRatio.setVal(newVal);
					optimalErr = err;
					optimalRatio.decreaseStep();
				} else
					break;
			}

			if(tmpOptimal == optimalRatio.getVal() && optimalRatio.getVal() <= 0.001)
				break;
		}

		OptimiserUtils.plotGraphs(countsFolderName, folderName, optimiserOut, this.bucketCalculator);
		FileUtils.deleteQuietly(countsFolder);

		return String.format("k=[%d,%d] and ratio=%f", optimalK[0], optimalK[1], optimalRatio.getVal());
	}

	private double testSBFRM(SketchTester tester, File countsFolder, int k1, int k2, double ratio) {
		Sketch sketch = this.factory.getSpectralBloomFilterRMSketch(k1, k2, ratio);
		try{
			tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(tester.getWriter().getCountsFolder(), sketch.getName(), "csv"));
			LOGGER.info(String.format(sketch.getName() + ": err=%f for k=[%d,%d] and ratio=%f", err, k1, k2, ratio));
			return err;
		} catch (Exception e) {
			LOGGER.error("Exception while testing a sketch " + sketch.getName(), e);
		}
		return Double.POSITIVE_INFINITY;
	}

	private double testDirection(SketchTester tester, File countsFolder, OptimisingDouble optimals, int[] optimalK, int dx, int dy, double optimalErr) {
		while(true) {
			double err = this.testSBFRM(tester, countsFolder, optimalK[0] + dx, optimalK[1] + dy, optimals.getVal());
			if(err <= optimalErr) {
				optimalErr = err;
				optimalK[0] += dx;
				optimalK[1] += dy;
				optimals.resetStep(0, 1);
			} else
				break;
		}
		return optimalErr;
	}

}
