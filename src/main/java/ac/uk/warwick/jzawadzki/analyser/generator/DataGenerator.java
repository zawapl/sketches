package ac.uk.warwick.jzawadzki.analyser.generator;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class DataGenerator {

	private final static Logger LOGGER = Logger.getLogger(DataGenerator.class);

	private File output;
	private DataGeneratorStatsWriter writer;
	private int size, range;
	private double bias, skew;
	//	skew - the degree to which the values cluster around the mode of the distribution; higher values mean tighter clustering
	//	bias - the tendency of the mode to approach the min, max or midpoint value; positive values bias toward max, negative values toward min

	public DataGenerator(File output, DataGeneratorStatsWriter writer, int size, int range) {
		this.output = output;
		this.writer = writer;
		this.size = size;
		this.range = range;
		this.bias = Math.exp(-0.1);
		this.skew = 0.5;
	}

	public void generate() {
		LOGGER.info("Generating random data");
		Random random = new Random(this.size);
		Map<Integer, int[]> counter = new HashMap<Integer, int[]>(this.range);
		//int[] counter = new int[this.range];
		long time = System.currentTimeMillis();
		double a = this.bias * this.range;
		ByteBuffer buffer = ByteBuffer.allocate(4);
		FileUtils.deleteQuietly(this.output);
		try ( FileOutputStream fOut = FileUtils.openOutputStream(this.output, false);
				BufferedOutputStream bOut = new BufferedOutputStream(fOut)) {
			for(int i = 0; i < this.size; i++) {
				if(i % 100000000 == 0) {
					bOut.flush();
					System.gc();
					LOGGER.info(i + " hashes processed");
				}
				int hash = this.randomNumber(random, a);
				buffer.rewind();
				bOut.write(buffer.putInt(hash).array());
				int[] count = counter.get(hash);
				if(count == null) {
					count = new int[1];
					counter.put(hash, count);
				}
				count[0]++;
			}
		} catch (IOException e){
			LOGGER.error("Exception while generating synthetic data", e);
		}
		LOGGER.info("Generatign random data finished");
		time = System.currentTimeMillis() - time;
		this.writer.saveCounterFile(counter);
		this.writer.saveStatsFile(time, counter);
	}

	protected int randomNumber(Random random, double a) {
		//		double unitGaussian = random.nextGaussian();
		//		double retval = (this.range / 2) + (this.range * (this.bias/ (this.bias + Math.exp(-unitGaussian/this.skew)) -0.5));
		//		return (int) Math.round(retval);
		return (int) Math.floor(a / (this.bias + Math.exp(-random.nextGaussian()/this.skew)));
	}

}
