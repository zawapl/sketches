package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class SpectralBloomFilterRM extends AbstractSimpleSketch {

	private MultiHasher hasher;
	private int[] primarySketch;
	private int[] secondarySketch;
	private int primarySize, secondarySize;
	private int k1, k2;

	public SpectralBloomFilterRM(int k1, int k2, int size1, int size2, MultiHasher hasher) {
		this.k1 = k1;
		this.k2 = k2;
		this.primarySize = size1;
		this.secondarySize = size2;
		this.hasher = hasher;
		this.primarySketch = new int[this.primarySize];
		this.secondarySketch = new int[this.secondarySize];
	}

	@Override
	public void add(int object) {
		int val = get(object) + 1;
		int count = 0;
		int id = -1;
		for(int i = 0; i < this.k1; i++) {
			int hash = this.hasher.hash(i, object, this.primarySize);
			if(this.primarySketch[hash] <= val){
				this.primarySketch[hash] = val;
				if(id != hash)
					count++;
				id = hash;
			}
		}
		if(count <= 1) { // If single minimal
			int min = this.secondarySketch[this.hasher.hash(0, object, this.secondarySize)];
			for(int i = 1; i < this.k2; i++) {
				int hash = this.hasher.hash(i, object, this.secondarySize);
				if(this.secondarySketch[hash] != 0 && this.secondarySketch[hash] < min)
					min = this.secondarySketch[hash];
			}
			if(min <= 0)
				min = val;
			else
				min++;
			for(int i = 0; i < this.k2; i++) {
				int hash = this.hasher.hash(i, object, this.secondarySize);
				this.secondarySketch[hash] = Math.max(this.secondarySketch[hash], min);
			}
		}
	}

	@Override
	public int get(int object) {
		int hash = this.hasher.hash(0, object, this.primarySize);
		int min = this.primarySketch[hash];
		int count = 1;
		int id = hash;
		for(int i = 1; i < this.k1; i++) {
			hash = this.hasher.hash(i, object, this.primarySize);
			int val = this.primarySketch[hash];
			if(val == min && id != hash)
				count++;
			else if(val < min) {
				min = val;
				count = 1;
				id = hash;
			}
		}
		if(count >= 2)
			return min;
		int min2 = this.secondarySketch[this.hasher.hash(0, object, this.secondarySize)];
		for(int i = 1; i < this.k2; i++)
			min2 = Math.min(min2, this.secondarySketch[this.hasher.hash(i, object, this.secondarySize)]);
		if(min2 > 0)
			return min2;
		return min;
	}

	@Override
	public String getName() {
		return "SBF-RM";//String.format("SBF-RM(r=%f, k=%d,%d)", this.secondarySize / (double) (this.secondarySize + this.primarySize), this.k1, this.k2);
	}

}
