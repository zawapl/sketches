package ac.uk.warwick.jzawadzki.analyser.optimiser;

public class OptimisingDouble {

	double val;
	double step;

	public OptimisingDouble(double val) {
		this.val = val;
		this.step = Math.min(val / 4, (1 - val) / 4);
	}

	public double getVal() {
		return this.val;
	}

	public void setVal(double val) {
		this.val = val;
	}

	public void resetStep(double min, double max) {
		this.step = Math.min(this.val - min, max - this.val) * 1.5;
	}

	public void decreaseStep() {
		this.step /= 2;
	}

	public double getStep() {
		return this.step;
	}

	@Override
	public String toString() {
		return Double.toString(this.val);
	}

}
