package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class SketchFactory {

	private MultiHasher hasher;
	private int m;

	public SketchFactory(int megabytes, MultiHasher hasher) {
		this.m = megabytes * 1024 * 1024 * 8;
		this.hasher = hasher;
	}

	public CountMin getCountMinSketch() {
		return this.getCountMinSketch(3);
	}

	public CountMin getCountMinSketch(int depth) {
		return new CountMin(depth, (this.m / depth) / 31, this.hasher);
	}

	public CountMinCU getCountMinCUSketch() {
		return this.getCountMinCUSketch(4);
	}

	public CountMinCU getCountMinCUSketch(int depth) {
		return new CountMinCU(depth, (this.m / depth) / 31, this.hasher);
	}

	public SpectralBloomFilter getSpectralBloomFilterSketch() {
		return this.getSpectralBloomFilterSketch(3);
	}

	public SpectralBloomFilter getSpectralBloomFilterSketch(int k) {
		return new SpectralBloomFilter(k, this.m / 31, this.hasher);
	}

	public SpectralBloomFilterCU getSpectralBloomFilterCUSketch() {
		return this.getSpectralBloomFilterCUSketch(4);
	}

	public SpectralBloomFilterCU getSpectralBloomFilterCUSketch(int k) {
		return new SpectralBloomFilterCU(k, this.m / 31, this.hasher);
	}

	public NaiveSketch getNaiveSketch() {
		return new NaiveSketch();
	}

	public SpectralBloomFilterRM getSpectralBloomFilterRMSketch() {
		return this.getSpectralBloomFilterRMSketch(4, 3, 0.25);
	}

	public SpectralBloomFilterRM getSpectralBloomFilterRMSketch(int k1, int k2, double ratio) {
		int counters = this.m / 31;
		int secondary = (int) (counters * ratio);
		return new SpectralBloomFilterRM(k1, k2, counters - secondary, secondary, this.hasher);
	}

	public BraidedSpectralBloomFilter getSpectralBloomFilterBraidedSketch() {
		//return getSpectralBloomFilterBraidedSketch(new double[]{0.075, 0.025, 0.009375}, new int[]{5,5,6,4});
		return this.getSpectralBloomFilterBraidedSketch(new double[]{0.075, 0.015625, 0.008203125}, new int[]{5,2,5,4});
	}

	public BraidedSpectralBloomFilter getSpectralBloomFilterBraidedSketch(double[] ratios, int[] ks) {
		int[] size = new int[ratios.length + 1];
		size[0] = this.m / 8;
		for(int i = 0; i < ratios.length; i++) {
			size[i + 1] = (int) (this.m * ratios[i] * 4);
			size[0] -= size[i + 1];
		}
		return new BraidedSpectralBloomFilter(this.hasher, ks, size);
	}

}
