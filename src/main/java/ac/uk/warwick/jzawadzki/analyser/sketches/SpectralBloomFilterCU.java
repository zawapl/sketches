package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class SpectralBloomFilterCU extends SpectralBloomFilter {

	public SpectralBloomFilterCU(int k, int size, MultiHasher hasher) {
		super(k, size, hasher);
	}

	@Override
	public void add(int object) {
		int val = get(object) + 1;
		for(int i = 0; i < this.k; i++) {
			int hash = this.hasher.hash(i, object, this.size);
			this.data[hash] = Math.max(val, this.data[hash]);
		}
	}

	@Override
	public String getName() {
		return "SBF-CU";//(k=" + this.k + ")";
	}

}
