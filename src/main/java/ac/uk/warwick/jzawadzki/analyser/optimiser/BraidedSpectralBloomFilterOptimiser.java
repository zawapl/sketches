package ac.uk.warwick.jzawadzki.analyser.optimiser;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.comparator.BucketCalculator;
import ac.uk.warwick.jzawadzki.analyser.sketches.Sketch;
import ac.uk.warwick.jzawadzki.analyser.sketches.SketchFactory;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchTester;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchWriter;

public class BraidedSpectralBloomFilterOptimiser {

	private final static Logger LOGGER = Logger.getLogger(BraidedSpectralBloomFilterOptimiser.class);

	private SketchFactory factory;
	private BucketCalculator bucketCalculator;
	private String folderName;
	private SketchTester tester;
	private File countsFolder;

	public BraidedSpectralBloomFilterOptimiser(SketchFactory factory, BucketCalculator bucketCalculator, String folderName, SketchTester tester, File countsFolder) {
		this.factory = factory;
		this.bucketCalculator = bucketCalculator;
		this.folderName = folderName;
		this.tester = tester;
		this.countsFolder = countsFolder;
	}

	public String optimiseBSBF(String optimiserOut) {
		int[] optimalK = {4,2,5,4};
		OptimisingDouble[] optimalRatios = {
				new OptimisingDouble(0.075),
				new OptimisingDouble(0.015625),
				new OptimisingDouble(0.008203125) // 0.008203125 for err=0.194490 
		};
		double optimalErr = Double.MAX_VALUE;

		while(true) {
			double tmpErr = optimalErr;
			for(int i = 0; i < optimalK.length; i++) {
				optimalErr = this.testIntDirection(optimalRatios, optimalK, 1, optimalErr, i);
				if(optimalErr != tmpErr)
					optimalErr = this.testIntDirection(optimalRatios, optimalK, -1, optimalErr, i);
			}

			if(optimalErr != tmpErr)
				for(int i = 0; i < optimalRatios.length; i++) {
					optimalRatios[i].decreaseStep();
					optimalErr = this.testDoubleDirection(optimalRatios, optimalK, 1, optimalErr, i);
					optimalErr = this.testDoubleDirection(optimalRatios, optimalK, -1, optimalErr, i);
				}

			double max = Double.MAX_VALUE;
			for(int i = 0; i < optimalRatios.length; i++)
				max = Math.min(optimalRatios[i].getStep(), max);

			if(max <= 1d / 128)
				break;
		}

		//OptimiserUtils.plotGraphs(optimiserOut + this.folderName + "counts", this.folderName, optimiserOut, this.bucketCalculator);
		FileUtils.deleteQuietly(this.countsFolder);

		return String.format("k=%s and ratios=%s", Arrays.toString(optimalK), Arrays.toString(optimalRatios));
	}

	private double testIntDirection(OptimisingDouble[] optimals, int[] optimalK, int dx, double optimalErr, int layer) {
		int[] tmpK = optimalK.clone();
		double[] ratios = new double[optimals.length];
		for(int i = 0; i < ratios.length; i++)
			ratios[i] = optimals[i].getVal();
		int tmpDx = dx;
		while(true) {
			int newVal = optimalK[layer] + tmpDx;
			tmpK[layer] = newVal;
			double err = this.testSBFB(tmpK, ratios);
			if(err < optimalErr) {
				optimalK[layer] = newVal;
				optimalErr = err;
				tmpDx = dx;
			} else if (err == optimalErr)
				tmpDx += dx;
			else
				break;
		}

		return optimalErr;
	}

	private double testDoubleDirection(OptimisingDouble[] optimals, int[] optimalK, int dx, double optimalErr, int layer) {
		double[] ratios = new double[optimals.length];
		double max = 1;
		for(int i = 0; i < ratios.length; i++) {
			ratios[i] = optimals[i].getVal();
			if(i != layer)
				max -= ratios[i];
		}

		while(true) {
			double newVal = optimals[layer].getVal() + (dx * optimals[layer].getStep());

			while(newVal >= max || newVal <= 0) {
				optimals[layer].decreaseStep();
				newVal = optimals[layer].getVal() + (dx * optimals[layer].getStep());
			}

			ratios[layer] = newVal;
			double err = this.testSBFB(optimalK, ratios);
			if(err < optimalErr) {
				optimals[layer].setVal(newVal);
				optimalErr = err;
			} else
				break;
		}

		return optimalErr;
	}

	private double testSBFB(int[] ks, double[] ratios) {
		Sketch sketch = this.factory.getSpectralBloomFilterBraidedSketch(ratios, ks);
		try{
			this.tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(this.countsFolder, sketch.getName(), "csv"));
			LOGGER.info(String.format("%s: err=%f for ks=%s and ratios=%s", sketch.getName(), err, Arrays.toString(ks), Arrays.toString(ratios)));
			FileUtils.deleteQuietly(this.countsFolder);
			return err;
		} catch (Exception e) {
			LOGGER.error("Exception while testing a sketch " + sketch.getName(), e);
		}
		return Double.POSITIVE_INFINITY;
	}

}
