package ac.uk.warwick.jzawadzki.analyser.optimiser;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.comparator.BucketCalculator;
import ac.uk.warwick.jzawadzki.analyser.comparator.Comparator;
import ac.uk.warwick.jzawadzki.analyser.comparator.GraphPlotter;

public class OptimiserUtils {

	private static Logger LOGGER = Logger.getLogger(OptimiserUtils.class);

	public static void plotGraphs(String countsFolderName, String folderName, String optimiserOut, BucketCalculator bucketCalculator) {
		String gnuplotOut = optimiserOut + folderName + "gnuplot";
		String timesFolderName = optimiserOut + folderName + "times";
		GraphPlotter plotter = new GraphPlotter(gnuplotOut);
		Comparator comparator = new Comparator(OptimiserUtils.getFiles(countsFolderName), timesFolderName, plotter, bucketCalculator);
		comparator.createAllGraphs(false);
	}

	public static File[] getFiles(String folder) {
		try {
			File dir = new File(folder);
			FileFilter fileFilter = new WildcardFileFilter("*");
			return dir.listFiles(fileFilter);
		} catch (SecurityException e){
			LOGGER.error("Failed to get files for " + folder, e);
		}
		return new File[0];
	}

	public static double evaluate(BucketCalculator singleBucket, File sketch) {
		double[][] errors;
		try(CSVParser parser = getParser(sketch)){
			errors = singleBucket.getErrorBuckets(parser, "Count", "True count");
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		double sum = errors[0][1];
		for(int i = 1; i < errors.length; i++)
			sum += errors[i][1];
		return sum / errors.length;
	}

	private static CSVParser getParser(File file) throws IOException{
		return CSVParser.parse(file, Charset.forName("UTF-8"), CSVFormat.DEFAULT.withHeader());
	}
}
