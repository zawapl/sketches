package ac.uk.warwick.jzawadzki.analyser.sketches;

import ac.uk.warwick.jzawadzki.analyser.utils.MultiHasher;

public class SpectralBloomFilter extends AbstractSimpleSketch {

	protected MultiHasher hasher;
	protected int[] data;
	protected int k;
	protected int size;

	public SpectralBloomFilter(int k, int size, MultiHasher hasher) {
		super();
		this.hasher = hasher;
		this.data = new int[size];
		this.k = k;
		this.size = size;
	}

	@Override
	public void add(int object) {
		for(int i = 0; i < this.k; i++)
			this.data[this.hasher.hash(i, object, this.size)]++;
	}

	@Override
	public int get(int object) {
		int min = this.data[this.hasher.hash(0, object, this.size)];
		for(int i = 1; i < this.k; i++)
			min = Math.min(min, this.data[this.hasher.hash(i, object, this.size)]);
		return min;
	}

	@Override
	public String getName() {
		return "SBF";//(k=" + this.k + ")";
	}

}
