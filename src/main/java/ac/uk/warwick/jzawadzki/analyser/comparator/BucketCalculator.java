package ac.uk.warwick.jzawadzki.analyser.comparator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

public class BucketCalculator {

	private static Logger LOGGER = Logger.getLogger(BucketCalculator.class);

	private int buckets;
	private int max;

	public BucketCalculator(int buckets, int max) {
		this.buckets = buckets;
		this.max = max;
	}

	public double[] getAverageError(Iterable<CSVRecord> records, String countColumn, String trueCountColumn) {
		List<Double> errors = new LinkedList<>();
		for(CSVRecord record: records){
			long count = Long.parseLong(record.get(countColumn));
			long trueCount = Long.parseLong(record.get(trueCountColumn));
			if(trueCount == 0)
				trueCount = 1;
			Double err = Math.abs(count - trueCount) / trueCount * 1.0;
			errors.add(err);
		}
		double sum = 0;
		for(Double err: errors)
			sum += err;
		double[] result = {sum / errors.size()};
		return result;
	}

	public double[][] getErrorBuckets(Iterable<CSVRecord> records, String countColumn, String trueCountColumn){
		double[][] buckets = this.getEmptyLogBuckets();
		int[] counts = new int[buckets.length];
		int rec = 0;
		int sum = 0;
		for(CSVRecord record: records){
			rec++;
			long count = Long.parseLong(record.get(countColumn));
			long trueCount = Long.parseLong(record.get(trueCountColumn));
			if(trueCount == 0)
				continue;

			sum += trueCount;

			double err = Math.abs(count - trueCount) * 100f / trueCount;
			for(int i = 0; i < buckets.length; i++)
				if(trueCount <= buckets[i][0]){
					counts[i]++;
					buckets[i][1] += err;
					break;
				}
		}

		LOGGER.info(rec + " records looked at with total sum of " + sum);
		for(int i = 0; i < buckets.length; i++) {
			if(buckets[i][1] != 0)
				buckets[i][1] /= counts[i];
		}
		return buckets;
	}

	public double[][] getBuckets(Iterable<CSVRecord> records, String countColumn){
		double[][] buckets = this.getEmptyLogBuckets();
		for(CSVRecord record: records){
			long count = Long.parseLong(record.get(countColumn));
			for(int i = 0; i < buckets.length; i++)
				if(count <= buckets[i][0]){
					buckets[i][1]++;
					break;
				}
		}
		return buckets;
	}

	private double[][] getEmptyLogBuckets(){
		double base = Math.pow(this.max, 1.0 / (this.buckets - 1));
		int p = 1;
		//int removedBuckets = 0;//(int) Math.ceil(-Math.log(base-1)/Math.log(base));
		//LOGGER.info("removedBuckets=" + removedBuckets);
		//double[][] limits = new double[this.buckets - removedBuckets][2];
		//		limits[0][0] = 1;
		//		for(int i = 1; i < limits.length; i++){
		//			while(Math.pow(base,p) < limits[i-1][0] + 1)
		//				p++;
		//			limits[i][0] = Math.pow(base,p);//, limits[i-1][0] + 1);
		//		}
		List<Integer> tmp = new ArrayList<>(this.buckets);
		tmp.add(1);
		while(Math.pow(base, p) < this.max) {
			while(Math.ceil(Math.pow(base, p)) < tmp.get(tmp.size() - 1) + 1)
				p++;
			tmp.add((int) Math.ceil(Math.pow(base, p)));
		}
		double[][] limits = new double[tmp.size()][2];
		for(int i = 0; i < limits.length; i++)
			limits[i][0] = tmp.get(i);
		return limits;

	}

}
