package ac.uk.warwick.jzawadzki.analyser.comparator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.optimiser.OptimiserUtils;
import ac.uk.warwick.jzawadzki.analyser.utils.Properties;

public class Comparator {

	private final static Logger LOGGER = Logger.getLogger(Comparator.class);

	private File[] sketchCounters;
	private GraphPlotter plotter;
	private BucketCalculator bucketCalculator;
	private String sketchTimesFolder;

	public Comparator(File[] sketchCounters, String sketchTimesFolder, GraphPlotter plotter, BucketCalculator bucketCalculator) {
		this.sketchCounters = sketchCounters;
		this.plotter = plotter;
		this.bucketCalculator = bucketCalculator;
		this.sketchTimesFolder = sketchTimesFolder;
	}

	public void createAllGraphs(boolean verify){
		boolean valid = verify ? this.verify() : true;

		if(valid){
			this.createFrequencyGraph();
			this.createFrequencyVsError();
			this.createTimeVsErrorsGraphs();
		}
	}

	private void createTimeVsErrorsGraphs(){
		LOGGER.info("Creating time-vs-errors graphs");
		Map<String, double[][]> buildData = new HashMap<>(this.sketchCounters.length);
		Map<String, double[][]> getData = new HashMap<>(this.sketchCounters.length);

		for(File sketch: this.sketchCounters){
			String name = sketch.getName().replace(".csv", "");
			LOGGER.info("Processing " + name);
			try(CSVParser parser = this.getParser(sketch)){
				//double[] quartiles = this.bucketCalculator.getErrorQuartiles(parser, "Count", "True count");
				//double[] quartiles = this.bucketCalculator.getAverageError(parser, "Count", "True count");
				double[] quartiles = { OptimiserUtils.evaluate(this.bucketCalculator, sketch) };
				double[][] build = new double[1][quartiles.length + 1];
				double[][] get = new double[1][quartiles.length + 1];
				System.arraycopy(quartiles, 0, build[0], 1, quartiles.length);
				System.arraycopy(quartiles, 0, get[0], 1, quartiles.length);
				build[0][0] = this.getTime(name, "build_time") / 1000f;
				get[0][0] = this.getTime(name, "get_time") / 1000f;
				getData.put(name, get);
				buildData.put(name, build);
			} catch (IOException e) {
				LOGGER.error("IOException when processing " + sketch, e);
			}
		}

		this.plotter.plotTimeVsErrors(getData, "get");
		this.plotter.plotTimeVsErrors(buildData, "build");
	}

	private int getTime(String sketchName, String property){
		Properties stats = new Properties();
		stats.load(this.sketchTimesFolder + File.separator + sketchName + ".txt");
		return stats.getInt(property, 0);
	}

	private void createFrequencyGraph(){
		LOGGER.info("Creating frequency graph");
		try(CSVParser parser = this.getParser(this.sketchCounters[0])){
			double[][] buckets = this.bucketCalculator.getBuckets(parser, "True count");
			double[][] data = new double[buckets.length][3];
			data[0][0] = buckets[0][0]/2;
			data[0][1] = buckets[0][1];
			data[0][2] = buckets[0][0];
			for(int i = 1; i < buckets.length; i++){
				data[i][0] = (buckets[i][0] + buckets[i-1][0])/2;
				data[i][1] = buckets[i][1];
				data[i][2] = buckets[i-1][0] - buckets[i][0];
			}

			this.plotter.plotFrequencyDistribution(data);
		} catch (IOException e) {
			LOGGER.error("IOException thrown when creating frequency graph", e);
		}
	}

	private void createFrequencyVsError(){
		LOGGER.info("Creating freq-vs-error graph");
		Map<String, double[][]> dataSets = new HashMap<>(this.sketchCounters.length);

		for(File sketch: this.sketchCounters){
			LOGGER.info("Processing " + sketch.getName());
			try(CSVParser parser = this.getParser(sketch)){
				double[][] errors = this.bucketCalculator.getErrorBuckets(parser, "Count", "True count");
				dataSets.put(sketch.getName().replace(".csv", ""), errors);
			} catch (IOException e) {
				LOGGER.error("IOException when processing " + sketch, e);
			}
		}

		this.plotter.plotFreqVsError(dataSets, "1:128", false, true);
		this.plotter.plotFreqVsError(dataSets, "1:", true, true);
		this.plotter.plotFreqVsError(dataSets, "64:", true, false);
	}

	private CSVParser getParser(File file) throws IOException{
		return CSVParser.parse(file, Charset.forName("UTF-8"), CSVFormat.DEFAULT.withHeader());
	}

	private boolean verify(){
		boolean valid = true;
		try(CSVParser countMinParser = this.getParser(this.sketchCounters[0]);
				CSVParser countMinCUParser = this.getParser(this.sketchCounters[1])){
			valid &= Verifier.verifyCountMinAndCountMinCU(countMinParser, countMinCUParser);
		} catch (IOException e) {
			LOGGER.warn("Verifying CountMin and CountMinCU failed due to a IOException");
		}
		return valid;
	}
}
