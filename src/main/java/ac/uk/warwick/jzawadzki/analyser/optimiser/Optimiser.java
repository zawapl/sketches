package ac.uk.warwick.jzawadzki.analyser.optimiser;

import java.io.File;

import org.apache.log4j.Logger;

import ac.uk.warwick.jzawadzki.analyser.comparator.BucketCalculator;
import ac.uk.warwick.jzawadzki.analyser.sketches.Sketch;
import ac.uk.warwick.jzawadzki.analyser.sketches.SketchFactory;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchTester;
import ac.uk.warwick.jzawadzki.analyser.tester.SketchWriter;

public class Optimiser {

	private final static Logger LOGGER = Logger.getLogger(Optimiser.class);

	private SketchFactory factory;
	private File[] inputFiles;
	private File counterFile;
	private String optimiserOut;
	private BucketCalculator bucketCalculator;

	public Optimiser(SketchFactory factory, File[] inputFiles, File counterFile, String optimiserOut,
			BucketCalculator bucketCalculator) {
		super();
		this.factory = factory;
		this.inputFiles = inputFiles;
		this.counterFile = counterFile;
		this.optimiserOut = optimiserOut;
		this.bucketCalculator = bucketCalculator;
	}

	public int optimiseCountMin() {
		int optimalK = 0;
		double optimalErr = Double.MAX_VALUE;

		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);

		SketchTester tester = getTester(folderName, countsFolder);
		for(int k = 1; k <= 10; k++) {
			Sketch sketch = this.factory.getCountMinSketch(k);
			tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(countsFolder, sketch.getName(), "csv"));
			LOGGER.info(sketch.getName() + ": err=" + err + " for k=" + k);
			if(err <= optimalErr) {
				optimalErr = err;
				optimalK = k;
			} else
				break;
		}

		OptimiserUtils.plotGraphs(countsFolderName, folderName, this.optimiserOut, this.bucketCalculator);

		return optimalK;
	}

	public int optimiseCountMinCU() {
		int optimalK = 0;
		double optimalErr = Double.MAX_VALUE;

		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);

		SketchTester tester = getTester(folderName, countsFolder);
		for(int k = 1; k <= 10; k++) {
			Sketch sketch = this.factory.getCountMinCUSketch(k);
			tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(countsFolder, sketch.getName(), "csv"));
			LOGGER.info(sketch.getName() + ": err=" + err + " for k=" + k);
			if(err <= optimalErr) {
				optimalErr = err;
				optimalK = k;
			} else
				break;
		}

		OptimiserUtils.plotGraphs(countsFolderName, folderName, this.optimiserOut, this.bucketCalculator);

		return optimalK;
	}

	public int optimiseSpectralBloom() {
		int optimalK = 0;
		double optimalErr = Double.MAX_VALUE;

		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);

		SketchTester tester = getTester(folderName, countsFolder);
		for(int k = 1; k <= 10; k++) {
			Sketch sketch = this.factory.getSpectralBloomFilterSketch(k);
			tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(countsFolder, sketch.getName(), "csv"));
			LOGGER.info(sketch.getName() + ": err=" + err + " for k=" + k);
			if(err <= optimalErr) {
				optimalErr = err;
				optimalK = k;
			} else
				break;
		}

		OptimiserUtils.plotGraphs(countsFolderName, folderName, this.optimiserOut, this.bucketCalculator);

		return optimalK;
	}

	public int optimiseSpectralBloomCU() {
		int optimalK = 0;
		double optimalErr = Double.MAX_VALUE;

		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);

		SketchTester tester = getTester(folderName, countsFolder);
		for(int k = 1; k <= 10; k++) {
			Sketch sketch = this.factory.getSpectralBloomFilterCUSketch(k);
			tester.test(sketch);
			double err = OptimiserUtils.evaluate(this.bucketCalculator, SketchWriter.getOutputFile(countsFolder, sketch.getName(), "csv"));
			LOGGER.info(sketch.getName() + ": err=" + err + " for k=" + k);
			if(err <= optimalErr) {
				optimalErr = err;
				optimalK = k;
			} else
				break;
		}

		OptimiserUtils.plotGraphs(countsFolderName, folderName, this.optimiserOut, this.bucketCalculator);

		return optimalK;
	}

	public String optimiseSpectralBloomRM() {
		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);
		SketchTester tester = getTester(folderName, countsFolder);
		SpectralBloomOptimiserRM optimiser = new SpectralBloomOptimiserRM(this.factory, this.bucketCalculator);
		return optimiser.optimiseSpectralBloomRM(this.optimiserOut, tester);
	}

	public String optimiseBraidedSpectralBloomRM() {
		String folderName = File.separator + Long.toString(System.currentTimeMillis()) + File.separator;
		String countsFolderName = this.optimiserOut + folderName + "counts";
		File countsFolder = new File(countsFolderName);
		SketchTester tester = getTester(folderName, countsFolder);
		BraidedSpectralBloomFilterOptimiser optimiser = new BraidedSpectralBloomFilterOptimiser(this.factory, this.bucketCalculator, folderName, tester, countsFolder);
		return optimiser.optimiseBSBF(this.optimiserOut);
	}

	private SketchTester getTester(String folderName, File countsFolder) {
		String timesFolderName = this.optimiserOut + folderName + "times";
		File timesFolder = new File(timesFolderName);

		SketchWriter writer = new SketchWriter(this.counterFile, countsFolder, timesFolder);
		SketchTester tester = new SketchTester(this.inputFiles, writer);
		return tester;
	}

}
